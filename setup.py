from setuptools import setup
import os

here = os.path.abspath(os.path.dirname(__file__))


setup(name='libtaskotron',
      version='0.0.3',
      description='base lib for taskotron jobs',
      author='Tim Flink',
      author_email='tflink@fedoraproject.org',
      license='GPLv2+',
      url='https://bitbucket.org/fedoraqa/libtaskotron-demo',
      packages=['libtaskotron'],
      package_dir={'libtaskotron':'libtaskotron'},
      include_package_data=True,
      entry_points=dict(console_scripts=['runtask=libtaskotron.runner:main']),
      install_requires = [
        'PyYAML',
     ]
     )
