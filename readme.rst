libtaskotron proof-of-concept 0.0.1
===================================

This is a proof-of-concept for libtaskotron. The purpose of this is to illustrate
where we're going with taskotron and get input on any perceived problems early on
before anything is final.

Please keep in mind that this is proof-of-concept code - there are a lot of things
which are hard-coded and nothing is final. Please don't develop against this code
unless you've talked to us first.

Please direct questions and comments to either `#fedora-qa` on freenode or the
`qa-devel mailing list <https://admin.fedoraproject.org/mailman/listinfo/qa-devel>`_.

Installing the Code
-------------------

For the moment, libtaskotron can't be fully installed by either pip or rpm and
needs a bit of both for now.

First, install some necessary packages::

  koji
  rpm-python
  pyOpenSSL
  gcc
  python-pycurl
  python-urlgrabber

Then, set up the virtualenv::

  virtualenv --system-site-packages env_taskotron
  source env_taskotron/bin/activate
  pip install -r requirements.txt

Finally, you should install libtaskotron, so that you don't need to specify the
full path for the taskotron runner every time you want to run it. You can either
use a standard installation, if you want to just use this project::

  pip install .

Or, if you intend to work on this project, you can install it in the editable
mode. This way you don't need to reinstall the project every time you make some
changes to it, the code changes are reflected immediately::

  pip install -e .

Running the Demo
----------------

For now, the only supported task is `rpmlint <https://bitbucket.org/fedoraqa/task-rpmlint>`_.

To run that task against some envr `<envr>` for some arch `<arch>`, do the
following::

  git clone https://bitbucket.org/fedoraqa/task-rpmlint.git
  runtask -e <envr> -a <arch> task-rpmlint/rpmlint.yml

This will download the `<envr>` from koji into a temp directory under `/var/tmp/`,
run rpmlint on the downloaded rpms and print output in TAP format to stdout.

Example::

  runtask -e xchat-2.8.8-21.fc20 -a x86_64 task-rpmlint/rpmlint.yml

Task Description Format
-----------------------

The task description format is mostly yaml with support for variable replacement::

    dependencies:
        - rpmlint
        - libtaskbot

    input:
        args: envr,arch

    preparation:
        koji: download $envr

    execution:
        python: run_rpmlint.py $workdir

    post:
        shell: clean $workdir

    report:
        resultdb: something

In this case, `$envr` is passed in to the runner as an argument and `$workdir`
is the directory in which the task is running.

For the moment, what you see in the rpmlint task is the limit of what is supported
for task description syntax.
