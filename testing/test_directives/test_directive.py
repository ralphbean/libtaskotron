from libtaskotron.directives import BaseDirective

class TestDirective(BaseDirective):
    def process(self, command, input_data, env_data):
        return "this is just a test"
