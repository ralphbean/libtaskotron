import pytest
import os
import sys

from libtaskotron import runner



class TestRunnerInputVerify():
    def test_yamlrunner_valid_input(self):
        ref_cmd = '-e foo-1.2-3.fc99 -a x86_64 footask.yml'
        ref_argdata = {'input': {'args': 'envr,arch'}}
        ref_data = {'envr': 'foo-1.2-3.fc99', 'arch': 'x86_64', 'taskfile': 'footask.yml'}

        test_parser = runner.get_argparser()
        test_args = vars(test_parser.parse_args(ref_cmd.split()))

        test_runner = runner.Runner(ref_argdata, test_args)

        test_runner._validate_input()

    def test_yamlrunner_fails_missing_arg(self):
        ref_cmd = '-e foo-1.2-3.fc99 footask.yml'
        ref_inputspec = {'args':'envr,arch'}

        test_parser = runner.get_argparser()
        test_args = vars(test_parser.parse_args(ref_cmd.split()))

        test_runner = runner.Runner(ref_inputspec, test_args)

        with pytest.raises(Exception):
            test_runner._validate_input()

class TestRunnerSetup():
    def test_trivial_creation(self, tmpdir):
        ref_taskdata = {'preparation': {'koji': 'download envr'},
                    'input': {'args': 'envr,arch'}, 'post': {'shell': 'clean'},
                    'execution': {'python': 'run_rpmlint.py'},
                    'dependencies': ['rpmlint', 'libtaskbot']}
        ref_inputdata = {}

        test_runner = runner.Runner(ref_taskdata, ref_inputdata, tmpdir)

        assert test_runner.taskdata == ref_taskdata

class TestRunnerPrep():
    def test_prep_invalid_commands(self, tmpdir):
        ref_taskdata = {'preparation': {'turboencabulator':'reduce sinusoidal repleneration'}}
        ref_inputdata = {}

        test_runner = runner.Runner(ref_taskdata, ref_inputdata, tmpdir)

        with pytest.raises(Exception):
            test_runner.do_prep()

class TestRunnerDirectiveLoader():

    def setup_method(self, method):
        self.ref_dir = os.path.abspath('testing/test_directives/')
        self.ref_taskdata = {}
        self.ref_inputdata = {}
        self.ref_directivename = 'test'

    def test_runner_load_directive(self, tmpdir):
        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata,
                                    tmpdir)

        test_runner._load_directive(self.ref_directivename, self.ref_dir)

        assert '%s_directive' % self.ref_directivename in sys.modules.keys()

    def test_runner_load_directive_indict(self, tmpdir):
        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata,
                                    tmpdir)

        test_runner._load_directive(self.ref_directivename, self.ref_dir)

        assert self.ref_directivename in test_runner.directives.keys()

    def test_runner_load_directive_notexist(self, tmpdir):
        self.ref_directivename = 'noexist'

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata,
                                    tmpdir)

        with pytest.raises(Exception):
            test_runner._load_directive(self.ref_directivename, self.ref_dir)

class TestRunnerSingleAction():

    def setup_method(self, method):
        self.ref_taskdata = {}
        self.ref_inputdata = {}

    def test_runner_extract_directive_name(self):
        ref_directivename = 'dummy'
        ref_action = {'name': 'test action',
                      ref_directivename: {'result': 'pass', 'msg': 'dummy message'}}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        test_directivename = test_runner._extract_directive_from_action(ref_action)

        assert test_directivename == ref_directivename

    def test_runner_single_dummy_action_pass(self):
        ref_action = {'dummy': {'result': 'pass'}}
        ref_action.update({'name': 'Dummy Action' })

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        test_runner.do_single_action(ref_action)


    def test_runner_single_dummy_action_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        with pytest.raises(Exception):
            test_runner.do_single_action(ref_action)

    def test_runner_do_single_dummy_action_pass_export(self):
        ref_exportname = 'dummy_output'
        ref_output = 'this is example output'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass', 'msg': ref_output},
                      'export': '%s' % ref_exportname}


        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        test_runner.do_single_action(ref_action)

        assert test_runner.working_data[ref_exportname] == ref_output

    def test_runner_do_single_dummy_action_pass_export_rendered_msg(self):
        ref_exportname = 'dummy_output'
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass',
                                'msg': '{{ %s }}' % ref_messagename},
                      'export': '%s' % ref_exportname}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)
        test_runner.working_data[ref_messagename] = ref_message

        test_runner.do_single_action(ref_action)

        assert test_runner.working_data[ref_exportname] == ref_message


class TestRunnerDoActions():

    def setup_method(self, method):
        self.ref_taskdata = {}
        self.ref_inputdata = {}

    def test_runner_do_single_dummy_pass(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass'}}
        self.ref_taskdata = {'task': [ref_action]}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        test_runner.do_actions()

    def test_runner_do_single_dummy_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}
        self.ref_taskdata = {'task': [ref_action]}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        with pytest.raises(Exception):
            test_runner.do_actions()

    def test_runner_do_multiple_dummy_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}
        self.ref_taskdata = {'task': [ref_action, ref_action]}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        with pytest.raises(Exception):
            test_runner.do_actions()

    def test_runner_do_multiple_dummy_pass_export_rendered_msg(self):
        ref_exportname = 'dummy_output'
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass',
                                'msg': '{{ %s }}' % ref_messagename},
                      'export': '%s' % ref_exportname}

        self.ref_taskdata = {'task': [ref_action, ref_action]}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)
        test_runner.working_data[ref_messagename] = ref_message

        test_runner.do_actions()

        assert test_runner.working_data[ref_exportname] == ref_message


class TestRunnerRenderAction():

    def setup_method(self, method):
        self.ref_taskdata = {}
        self.ref_inputdata = {}

    def test_runner_render_single_variable(self):
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'test dummy action', 'dummy': {'result': 'pass', 'msg': '{{ %s }}' % ref_messagename}}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)
        test_runner.working_data[ref_messagename] = ref_message

        test_rendered_action = test_runner._render_action(ref_action)

        assert test_rendered_action['dummy']['msg'] == ref_message


