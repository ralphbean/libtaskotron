import yaml

from libtaskotron import taskyaml

ref_filename = '/home/testuser/testtask.yml'
ref_taskname = 'testtask'
ref_taskdesc = 'this is a test task'
ref_maintainer = 'testuser'
ref_deps = ['libtaskotron', 'libpanacea']
ref_input = {'args': 'arch, envr'}
ref_actions = []


def create_yamldata(name=ref_taskname, desc=ref_taskdesc, maint= ref_maintainer,
                    deps=ref_deps, input=ref_input, actions=ref_actions):
    return {'name': name,
            'desc': desc,
            'maintainer': maint,
            'deps': deps,
            'input': input,
            'task': actions}


class TestTaskYamlMetadata(object):
    def setup_method(self, method):
        self.taskdata = create_yamldata()
        self.test_task = taskyaml.parse_yaml(yaml.safe_dump(self.taskdata))

    def test_taskyaml_taskname(self):
        assert self.test_task['name'] == self.taskdata['name']

    def test_taskyaml_taskdesc(self):
        assert self.test_task['desc'] == self.taskdata['desc']

    def test_taskyaml_taskmaintainer(self):
        assert self.test_task['maintainer'] == self.taskdata['maintainer']

    def test_taskyaml_taskdeps(self):
        assert self.test_task['deps'] == self.taskdata['deps']

    def test_taskyaml_taskinput(self):
        assert self.test_task['input'] == self.taskdata['input']


class TestTaskYamlActionsNoVariables(object):
    def test_taskyaml_simple_action(self):
        ref_action = {'name': 'testaction', 'action': {'key': 'value'}}
        taskdata = create_yamldata(actions=[ref_action])

        test_task = taskyaml.parse_yaml(yaml.safe_dump(taskdata))

        assert test_task['task'][0] == ref_action

    def test_taskyaml_multiple_actions(self):
        ref_action = {'name': 'testaction', 'action': {'key': 'value'}}
        taskdata = create_yamldata(actions=[ref_action, ref_action, ref_action])

        test_task = taskyaml.parse_yaml(yaml.safe_dump(taskdata))

        assert len(test_task['task']) == 3
