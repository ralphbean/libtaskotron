import pytest
from dingus import Dingus

from libtaskotron.directives import koji_directive

class TestKojiDownloads():
    def setup_method(self, method):
        self.ref_nvr = 'foo-1.2-3.fc99'
        self.ref_arch = 'noarch'
        self.ref_name = 'foo'
        self.ref_version = '1.2'
        self.ref_release = '3.fc99'
        self.ref_buildid = 123456

        self.ref_build = {'package_name':self.ref_name, 'version':self.ref_version,
                     'release':self.ref_release, 'id': self.ref_buildid}

        self.ref_rpms = [{'name':self.ref_name, 'version':self.ref_version, 'release':self.ref_release, 'nvr':self.ref_nvr, 'arch':self.ref_arch},
                    {'name':self.ref_name, 'version':self.ref_version, 'release':self.ref_release, 'nvr':self.ref_nvr, 'arch':'src'}]

        self.ref_url = 'http://downloads.example.com/builds/%s.noarch.rpm' % self.ref_nvr
        self.helper = koji_directive.KojiDirective()

    def test_get_noarch_rpmurls_from_nvr(self):
        stub_koji = Dingus(getBuild__returns = self.ref_build, listRPMs__returns = self.ref_rpms)

        test_koji = koji_directive.KojiDirective(stub_koji)

        test_urls = test_koji.nvr_to_urls(self.ref_nvr)

        koji_baseurl = koji_directive.pkg_url
        ref_urls = ['%s/%s/%s/%s/%s/%s.%s.rpm' % (koji_baseurl, self.ref_name, self.ref_version, self.ref_release, self.ref_arch, self.ref_nvr, self.ref_arch),
                    '%s/%s/%s/%s/src/%s.src.rpm' % (koji_baseurl, self.ref_name, self.ref_version, self.ref_release, self.ref_nvr),
                    ]

        assert test_urls == ref_urls

    def should_i386_rpmurls_query_i686(self):
        self.ref_arch = 'i386'

        stub_koji = Dingus(getBuild__returns = self.ref_build)

        test_koji = koji_directive.KojiDirective(stub_koji)

        # this isn't setup to actually return urls since we don't care about that for this test
        # it should throw an exception in this case.
        try:
            test_koji.nvr_to_urls(self.ref_nvr, arches = [self.ref_arch])
        except Exception:
            pass

        listrpm_calls = stub_koji.calls('listRPMs')
        requested_arches = listrpm_calls[0][2]['arches']

        assert 'i686' in requested_arches
        assert 'i386' in requested_arches

    def test_parse_download_command(self, monkeypatch):
        ref_input = {'action': 'download', 'arch': self.ref_arch, 'envr': self.ref_nvr}
        ref_envdata = {'workdir':'/var/tmp/foo'}

        stub_koji = Dingus(getBuild__returns = self.ref_build, listRPMs__returns = self.ref_rpms)
        ref_rpms = ['/var/tmp/fake/%s.%s.rpm' % (self.ref_nvr, self.ref_arch)]

        stub_get_rpms = Dingus(get_nvr_rpms__returns=ref_rpms)
        test_helper = koji_directive.KojiDirective(stub_koji)

        monkeypatch.setattr(test_helper, 'get_nvr_rpms', stub_get_rpms)
        test_helper.process(ref_input, ref_envdata)

        getrpm_calls = stub_get_rpms.calls()
        print getrpm_calls
        requested_nvr = getrpm_calls[0][1][0]

        assert len(getrpm_calls) == 1
        assert requested_nvr == self.ref_nvr

    def should_throw_exception_norpms(self):
        stub_koji = Dingus(getBuild__returns = self.ref_build)

        test_koji = koji_directive.KojiDirective(stub_koji)

        with pytest.raises(Exception):
            test_koji.nvr_to_urls(self.ref_nvr, arches = [self.ref_arch])

