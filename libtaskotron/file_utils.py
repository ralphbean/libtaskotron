import os
import sys
import urlgrabber.grabber, urlgrabber.progress
from libtaskotron.logger import log


def makedirs(fullpath):
    '''This is the same as @method os.makedirs(), but does not throw an
    exception when the destination directory already exists.'''
    try:
        os.makedirs(fullpath)
        assert os.path.isdir(fullpath)
    except OSError, e:
        if e.errno == 17: # "[Errno 17] File exists"
            # if it is a directory everything is ok
            if os.path.isdir(fullpath):
                return
            # otherwise it is a file/socket/etc and it is an error
            else:
                raise
        else:
            raise

def get_urlgrabber(**kwargs):
    '''Get a new instance of URLGrabber that is reasonably set (progress
       bar when in terminal, automatic retries, connection timeout, etc)
       @kwargs additional kwargs to add to the URLGrabber constructor
       @return URLGrabber instance
    '''
    grabber_args = {'retry': 3, 'timeout': 120}
    grabber_args.update(kwargs)

    grabber = urlgrabber.grabber.URLGrabber(**grabber_args)

    # progress bar when in terminal
    if hasattr(sys.stdout, "fileno") and os.isatty(sys.stdout.fileno()):
        grabber.opts.progress_obj = urlgrabber.progress.TextMeter()

    # retry on socket timeout
    if 12 not in grabber.opts.retrycodes:
        grabber.opts.retrycodes.append(12)

    return grabber


def download(url, dest, overwrite=False, grabber=None):
    '''Download a file.

    @param url file URL to download
    @param dest either an existing directory (string) or a file path (string).
                In the latter case all parent directories will be created if
                necessary and the last fraction of the path will be used as the
                file name.
    @param overwrite if the destination file already exists, whether to
                    overwrite or not. If False, a simple check is performed
                    whether the remote file is the same as the local file and
                    re-downloads it anyway if they are not the same.
    @param grabber custom urlgrabber.grabber.URLGrabber object
    @return file path of the downloaded file (string)
    @throw urlgrabber.grabber.URLGrabError if download fails
    '''

    if not grabber:
        grabber = get_urlgrabber()

    # get destination file, create dirs if needed
    if os.path.isdir(dest):
        dest = os.path.join(dest, os.path.basename(url))
    else:
        makedirs(os.path.dirname(dest))

    # overwrite?
    if os.path.exists(dest):
        if overwrite:
            os.remove(dest)
        else:
            log.debug('Already downloaded: %s', os.path.basename(dest))
            return dest

    log.info('Downloading: %s', url)
    try:
        grabber.urlgrab(url, dest)
    except urlgrabber.grabber.URLGrabError, e:
        log.error('Download error: %s', e)
        # the file can be incomplete, remove
        if os.path.exists(dest):
            try:
                os.remove(dest)
            except:
                log.error('Could not delete incomplete file: %s', dest)
        raise

    return dest

