#!/usr/bin/python
#
# Copyright 2014, Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Author: Martin Krizek <mkrizek@redhat.com>

import sys
import logging
import logging.handlers
import traceback


# http://docs.python.org/2/howto/logging.html#configuring-logging-for-a-library
log = logging.getLogger('libtaskotron')
log.addHandler(logging.NullHandler())


def _log_excepthook(*exc_info):
    '''Called when an exception is not caught'''
    log.critical(''.join(traceback.format_exception(*exc_info)))


def init(name='libtaskotron', level=logging.INFO, stream=True, syslog=False, filelog=None):
    """Setup a logger
    @param name name of the logger, 'libtaskotron' is default
    @param level level of logging
    @param stream enables logging to stderr
    @param syslog enables logging to syslog
    @param filelog enables logging to a file, the value is the file name
    """

    # overwrite default except hook
    sys.excepthook = _log_excepthook

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(logging.NullHandler())

    fmt = '[%(name)s:%(filename)s:%(lineno)d] %(asctime)s %(levelname)-7s %(message)s'
    datefmt = '%Y-%m-%d %H:%M:%S'
    formatter = logging.Formatter(fmt=fmt, datefmt=datefmt)

    if stream:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)
        logger.debug("doing stream logging")

    if syslog:
        syslog_handler = logging.handlers.SysLogHandler(
            address='/dev/log', facility=logging.handlers.SysLogHandler.LOG_LOCAL4)
        syslog_handler.setFormatter(formatter)
        logger.addHandler(syslog_handler)
        logger.debug("doing syslog logging")

    if filelog:
        file_handler = logging.handlers.RotatingFileHandler(filelog,
            maxBytes=500000, backupCount=5)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        logger.debug('doing file logging to %s' % filelog)

    return logger
