import logging
import tempfile
import os.path
import argparse
import imp
from jinja2 import Template, Environment
import copy

from libtaskotron import taskyaml
from libtaskotron import logger
from libtaskotron.logger import log

class Runner:
    def __init__(self, taskdata, argdata, workdir=None):
        self.taskdata = taskdata
        self.envdata = argdata
        self.working_data = {}
        self.directives = {}
        self.jinja_env = None

        if workdir is None:
            self.workdir = tempfile.mkdtemp(dir='/var/tmp')
        else:
            self.workdir = workdir

    def run(self):
        self._validate_input()
        self.envdata['workdir'] = self.workdir

        self.do_actions()

    def _load_directive(self, directive_name, directive_dir=None):
        # look in default path if nothing is specified
        if not directive_dir:
            directive_dir = os.path.join(os.path.dirname(__file__),
                                         'directives')

        real_name = "%s_directive" % directive_name
        directive_file = os.path.join(directive_dir, '%s.py' % real_name)

        if not os.path.exists(directive_file):
            raise Exception("Directive %s not found in directory %s" %
                            (directive_name, directive_dir))

        loaded_directive = imp.load_source(real_name, directive_file)
        self.directives[directive_name] = loaded_directive

    def _get_jinja_env(self):
        if not self.jinja_env:
            self.jinja_env = Environment()
            self.jinja_env.globals = self.envdata
        return self.jinja_env

    def _render_action(self, action):
        # copy the input so that we don't disrupt what we're processing
        rendered_action = copy.deepcopy(action)

        # any action is going to be a dict with 0 or more embedded dicts
        for action_key in action:
            jinja_env = self._get_jinja_env()

            if isinstance(action[action_key], dict):
                action_line = action[action_key]
                for keyval in action_line:
                    input_action = action[action_key][keyval]
                    arg_template = jinja_env.from_string(input_action)

                    rendered_line = arg_template.render(self.working_data)
                    rendered_action[action_key][keyval] = rendered_line

        return rendered_action

    def _extract_directive_from_action(self, action):
        for key in action:
            if key not in ['name', 'export']:
                return key
        raise Exception('no directive found in action %s' % str(action))

    def do_single_action(self, action):
        directive_name = self._extract_directive_from_action(action)

        rendered_action = self._render_action(action)

        self._load_directive(directive_name)

        directive_object = self.directives[directive_name]
        directive_classname = directive_object.directive_class

        directive_callable = getattr(directive_object, directive_classname)()

        output = directive_callable.process(rendered_action[directive_name],
                                            self.envdata)

        if 'export' in action:
            self.working_data[action['export']] = output

    def do_actions(self):
        for action in self.taskdata['task']:
            self.do_single_action(action)

    def _validate_input(self):
        required_input = self.taskdata['input']['args'].split(',')

        for arg in required_input:
            if not arg in self.envdata:
                raise Exception('Required input arg %s was not defined' % arg)

    def _validate_env(self):
        raise NotImplementedError("Environment validation is not yet implemented")


def get_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("task", nargs=1, help="task to run")
    parser.add_argument("-a", "--arch", choices=["i386", "x86_64", "armhfp", "noarch"], help="""
                        Architecture to be used; support arches are x86, x86_64, and armhfp.
                        If omitted, defaults to noarch.
                        """)
    parser.add_argument("-e", "--envr", help="envr to be used as input")
    return parser

def main():
    parser = get_argparser()
    args = parser.parse_args()

    logger.init(level=logging.DEBUG)

    arg_data = vars(args)
    arg_data['taskfile'] = args.task[0]

    task_data = taskyaml.parse_yaml_from_file(arg_data['taskfile'])

    task_runner = Runner(task_data, arg_data)
    task_runner.run()

    for output in task_runner.working_data:
        log.info(task_runner.working_data[output])
