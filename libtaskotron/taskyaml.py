import yaml


def parse_yaml_from_file(filename):
    try:
        datafile = open(filename, 'r')
        data = parse_yaml(datafile.read())
    finally:
        datafile.close()

    return data


def parse_yaml(contents):
    return yaml.safe_load(contents)

