class BaseDirective(object):

    def __init__(self):
        pass

    def process(self, input_data, env_data):
        raise NotImplementedError

