import koji

from libtaskotron import file_utils
from libtaskotron.logger import log
from libtaskotron.directives import BaseDirective

directive_class = 'KojiDirective'

koji_url = 'http://koji.fedoraproject.org/kojihub'
pkg_url = 'http://kojipkgs.fedoraproject.org/packages'


class KojiDirective(BaseDirective):
    """The koji directive interfaces with Koji to facilitate various koji actions.

    format: "koji: command=[download] <command args>

    Valid Commands:

    download
      Downloads the envr specified as "envr"
      format: "koji: command=download envr=<envr to download> arch=<desired arch>
    """

    def __init__(self, koji_session=None):
        self.session = koji_session

    def get_koji_session(self):
        if self.session == None:
            self.session = koji.ClientSession(koji_url)

        return self.session

    def nvr_to_urls(self, nvr, arches=None, debuginfo=False, src=True):
        '''Get list of URLs for RPMs corresponding to a build.
        @param nvr build NVR
        @param arches restrict the arches of builds to provide URLs for. If
                        basearch i386 is in the list, i686 arch is automatically
                        added (since that's the arch Koji API uses).
        '''

        ksession = self.get_koji_session()

        # add i686 arch if i386 is present in arches
        if arches and 'i386' in arches and 'i686' not in arches:
            arches.append('i686')

        info = ksession.getBuild(nvr)
        if info == None:
            raise Exception("No build information found for %s" % nvr)

        baseurl = '/'.join((pkg_url, info['package_name'],
                            info['version'], info['release']))

        rpms = ksession.listRPMs(buildID=info['id'], arches=arches)
        if not debuginfo:
            rpms = filter(lambda r: not r['name'].endswith('-debuginfo'), rpms)
        if not src:
            rpms = filter(lambda r: not r['arch'] == 'src', rpms)

        urls = ['%s/%s' % (baseurl, koji.pathinfo.rpm(r)) for r in rpms]
        if len(urls) == 0:
            raise Exception("NO RPMS URLS FOUND!!!")

        return sorted(urls)

    def get_nvr_rpms(self, nvr, rpm_dir, arches=None, debuginfo=False, src=False):
        '''Retrieve the RPMs associated with a build NVR into the specified
            directory.

        @param nvr build NVR
        @param rpm_dir location where to store the RPMs
        @return list of local filenames of the grabbed RPMs
        @raise urlgrabber.grabber.URLGrabError if downloading failed
        '''

        rpm_urls = self.nvr_to_urls(nvr,
                                    arches=arches,
                                    debuginfo=debuginfo,
                                    src=src)

        rpm_files = []
        log.info('Fetching RPMs for: %s', nvr)
        for url in rpm_urls:
            log.info('  fetching %s', url)
            rpm_file = file_utils. download(url, rpm_dir)
            rpm_files.append(rpm_file)

        return rpm_files

    def process(self, input_data, env_data):
        valid_actions = ['download']

        output_data = {}

        action = input_data['action']

        if not action in valid_actions:
            raise Exception('%s is not a valid command for koji helper' % action)

        # for now, the only valid command is download, can simplify code

        if action == 'download':
            if 'envr' in input_data and 'arch' in input_data:
                # we need to find the tmpdir and nvr from input data
                tmpdir = env_data['workdir']
                envr = input_data['envr']
                arches = [input_data['arch']]

                log.info("getting koji builds for %s (%s) and downloading to %s", envr, arches, tmpdir)
                output_data['downloaded_rpms'] = self.get_nvr_rpms(envr,
                                                                   tmpdir,
                                                                   arches)

        return output_data
