import imp
import os

from libtaskotron.directives import BaseDirective

directive_class = 'PythonDirective'


class PythonDirective(BaseDirective):
    """The python directive is designed to execute a piece of python code as part
    of task execution. Given a callable name 'dothis_thing', possible implementations
    in the loaded code could include:

        class TaskClass(object):
            def my_task(self):
                return "I'm a task class method!"

        _instantiated_class = TaskClass()

        task_class_target = _instantiated_class.my_task

    In this case, you could pass in 'task_class_target' as the callable arg

        class EmbeddedCallClass(object):
            def __call__(self):
                return "I'm a __call__ method in a class!"

        embedded_task_target = EmbeddedCallClass()

    In this case, you could pass in 'embedded_task_target' as the callable arg

        def task_method():
            return "I'm a task method!"

    And in this third case, you would pass in 'task_method' as the callable arg

    format:
      python: file=<python filename> callable=<method name> [<kwargname>=<kwargvalue>'
      required params: <python filename>, callable
      optional params: farther key/value pairs, to be passed into the callable as kwargs
    """

    def _do_getattr(self, module, name):
        """Isolation of getattr to make the code more easily testable

        @param module module from which to getattr from
        @param name name of object to getattr
        @returns object retrieved from module
        """

        return getattr(module, name)

    def execute(self, task_module, method_name, kwargs):
        """Execute a callable in the specified module

        @param task_module module containing the specified callable
        @param method_name name of callable to execute
        @param kwargs kwargs to pass as parameters into the callable
        @return output from the executed method
        """

        task_method = self._do_getattr(task_module, method_name)

        output = task_method(**kwargs)

        return output

    def load_pyfile(self, filename):
        """Import python code specified

        @param filename absolute path to the python file
        """

        task_importname = 'runtask_%s' % os.path.basename(filename).rstrip('.py')
        task = imp.load_source(task_importname, filename)

        return task_importname, task

    def checkfile(self, filename):
        """Check to see if the file exists

        @param filename abspath to the filename
        @throws Exception if the file does not exist
        """

        if not os.path.exists(filename):
            raise Exception("The file %s does not exist!" % filename)

    def process(self, input_data, env_data):
        """For the python directive, we're expecting a yaml declaration of the form
        "python: file=<python file> callable=<method name> [kwarg1=something ...]"

        @param input_data dictionary of all kwargs from yaml declaration, including callable and file
        @param env_data information on the environment in which we're running
        @return output from the method specified, this is expected to be TAP output
        """

        if 'callable' not in input_data or 'file' not in input_data:
            detected_args = ', '.join(input_data.keys())
            raise Exception("The python directive requires both 'callable' and \
'file' arguments. Detected arguments: %s" % detected_args)

        method_name = input_data.pop('callable')
        basedir = os.path.dirname(env_data['taskfile'])

        pyfile = os.path.join(basedir, input_data.pop('file'))

        self.checkfile(pyfile)

        task_name, task = self.load_pyfile(pyfile)

        output = self.execute(task, method_name, input_data)

        return output
