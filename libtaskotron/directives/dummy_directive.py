from libtaskotron.directives import BaseDirective

directive_class = 'DummyDirective'


class DummyDirective(BaseDirective):
    """The dummy directive is just what it sounds like - a directive that
    doesn't do anything other than return a status and (optionally) a message.

    It is primarially meant for testing the runner or as a placeholder while
    writing new tasks.

    format:
      dummy: result=<result> [msg=<some string message>]
      required params: result
      optional params: msg
    """

    def process(self, input_data, env_data):
        expected_result = input_data['result']

        if expected_result.lower() == 'fail':
            raise Exception("This is a dummy directive and configured to fail")

        if 'msg' in input_data:
            return input_data['msg']
