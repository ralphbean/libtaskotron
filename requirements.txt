PyYAML==3.10
dingus==0.3.4
py>=1.4.18
pyaml==13.07.1
pytest>=2.4.2
jinja2>=2.7.2

# these are bundled in libtaskotron for now
#bayeux==0.7
#yamlish==0.10
